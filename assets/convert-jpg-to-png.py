from PIL import Image
import os

directory = r'C:\Users\andre\Desktop\the-rpg-thing\assets\images\locations'
c=1
for filename in os.listdir(directory):
    print(filename)
    if filename.endswith(".jpg") or filename.endswith(".jpeg"):
        im = Image.open(os.path.join(directory, filename))
        name='img'+str(c)+'.png'
        rgb_im = im.convert('RGB')
        rgb_im.save(name)
        c+=1
        continue
    else:
        continue