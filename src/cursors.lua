cursors = {
    combat = {
        v = 1,
        l = 4,
    },
    combatPage = {
        v = 1,
        l = 2
    },
    combatItem = {
        v = 1,
        l = 1
    },
    combatSpell = {
        v = 1,
        l = 1
    },
    menu = {
        v = 1,
        l = 4
    },
    pause = {
        v = 1,
        l = 3
    },
    inventory = {
        v = 1,
        l = 3
    },
    inventoryItem = {
        v = 1,
        l = 1
    },
    inventorySpell = {
        v = 1,
        l = 1
    },
    inventoryPage = {
        v = 1,
        l = 2
    }
}

function resetCursor(c)
    cursors[c].v = 1
end

function cDown(c)
    cursors[c].v = cursors[c].v + 1
    if cursors[c].v > cursors[c].l then
        cursors[c].v = 1
    end
end

function cRight(c)
    cDown(c)
end

function cUp(c)
    cursors[c].v = cursors[c].v - 1
    if cursors[c].v < 1 then
        cursors[c].v = cursors[c].l
    end
end

function cLeft(c)
    cUp(c)
end