local dbox = 1
local flesh = 3
local up = 8
local down = 2
local left = 5
local right = 6
local cont = 9

function love.draw()
    love.graphics.setColor(1,1,1)
    if screenState.isLoading then
        love.graphics.setFont(serif)
        love.graphics.print('Loading...', 20, 20)
    elseif screenState.isMenu then
        --temp
    elseif screenState.isPaused then
        pausedShader(function()
            local img = assets.images.locations[map[gameState.location].bgImg]
            love.graphics.draw(img,
            0, 0, 0, width/img:getWidth(), height/img:getHeight());
        end)
        love.graphics.setFont(vcr)
        love.graphics.print('PAUSED', 20, 20)
        love.graphics.print('  RESUME', 20, 80 , 0, 0.5)
        love.graphics.print('  SAVE',   20, 100 , 0, 0.5)
        love.graphics.print('  EXIT',   20, 120, 0, 0.5)
        love.graphics.draw(assets.images.sprites[cont], 20, 60 + (cursors.pause.v*20), 0, 0.5)
    else
        love.graphics.setFont(serif)
        if screenState.gameOver then
            local img = assets.images.sprites[13]
            love.graphics.draw(img,
            0, 0, 0, width/img:getWidth(), height/img:getHeight())
            do return end
        end
        love.graphics.setCanvas(bgcanvas)
        pixelshader(function()
            local img = assets.images.locations[map[gameState.location].bgImg]
            love.graphics.draw(img,
            0, 0, 0, width/img:getWidth(), height/img:getHeight())
        end)
        love.graphics.setCanvas()
        ambientShader(function()
            love.graphics.draw(bgcanvas, 0,0)

            if gameState.isInBattle then
                local sprite = assets.images.sprites[gameState.opponent.sprite]
                love.graphics.draw(sprite, (width/2)-(sprite:getWidth()/2), height-sprite:getHeight())
            end

            if screenState.isInDialogue or gameState.isInBattle or screenState.isInInventory then
                love.graphics.setColor(0,0,0,0.2)
                love.graphics.rectangle('fill',0,0,width,height)
                love.graphics.setColor(1,1,1)
                local imgwidth = assets.images.sprites[dbox]:getWidth()
                local imgheight = assets.images.sprites[dbox]:getHeight()
                local anchorx = (width/2)-(imgwidth/2)
                local anchory = height-(imgheight/2)-10
                love.graphics.draw(assets.images.sprites[dbox], anchorx, anchory, 0, 1, 0.5)
                if gameState.speaking and #gameState.speaking ~= 0 then
                    love.graphics.setColor(gameState.speaking[gameState.speakingIndex].color)
                    love.graphics.setFont(serif)
                    love.graphics.printf(
                        gameState.speaking[gameState.speakingIndex].text
                        , anchorx+20, anchory+15, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                    love.graphics.setColor(1,1,1)
                    love.graphics.draw(assets.images.sprites[cont], anchorx+imgwidth-55, anchory+(imgheight/2)-55)
                elseif not screenState.isInInventory then
                    love.graphics.setFont(vcr)
                    if gameState.turn then
                        love.graphics.setColor(1,0,0)
                        love.graphics.setFont(serif)
                        love.graphics.printf(
                            gameState.skin.." Skin  "..gameState.mind.." Mind", 
                            anchorx+22, anchory+(imgheight/2)-45, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                        if cursors.combatPage.v == 1 then
                            love.graphics.setColor(1,1,1)
                            love.graphics.setFont(serif)
                            love.graphics.printf(
                                quip, anchorx+20, anchory+15, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                            love.graphics.setColor(0.5,0.5,1)
                            love.graphics.setFont(vcr)
                            love.graphics.printf(
                                "  ATTCK   ITEMS   SPELL   LEAVE", 
                                anchorx+20, anchory+(imgheight/2)-75, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                            love.graphics.setColor(1,1,1)
                            love.graphics.draw(assets.images.sprites[cont], anchorx+20 + (cursors.combat.v-1)*115, anchory+(imgheight/2)-75, 0, 0.5)
                        else
                            if cursors.combat.v == 2 then
                                love.graphics.setColor(0.5,0.5,1)
                                local item = {}
                                for i=1, #gameState.inventory do
                                    item[i] = gameState.inventory[i]
                                end
                                local shift = cursors.combatItem.v
                                if shift < 4 then shift = 1 else shift = shift - 3 end
                                if item[shift  ] == nil then item[shift  ] = "NONE" end
                                if item[shift+1] == nil then item[shift+1] = "" end
                                if item[shift+2] == nil then item[shift+2] = "" end
                                if item[shift+3] == nil then item[shift+3] = "" end
                                love.graphics.setFont(vcr)
                                love.graphics.printf(
                                    "  "..item[shift].."   "..item[shift+1].."   "..item[shift+2].."   "..item[shift+3],
                                    anchorx+20, anchory+(imgheight/2)-75, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                                love.graphics.setColor(1,1,1)
                                love.graphics.setFont(serif)
                                love.graphics.printf(
                                    items[item[cursors.combatItem.v]].desc, anchorx+20, anchory+15, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                                local cursorsplacement = cursors.combatItem.v
                                if cursorsplacement > 4 then cursorsplacement = 4 end
                                love.graphics.draw(assets.images.sprites[cont], anchorx+20 + (cursorsplacement-1)*115, anchory+(imgheight/2)-75, 0, 0.5)
                            elseif cursors.combat.v == 3 then
                                love.graphics.setColor(0.5,0.5,1)
                                local item = {}
                                for i=1, #gameState.spells do
                                    item[i] = gameState.spells[i]
                                end
                                local shift = cursors.combatSpell.v
                                if shift < 4 then shift = 1 else shift = shift - 3 end
                                if item[shift  ] == nil then item[shift  ] = "NONE" end
                                if item[shift+1] == nil then item[shift+1] = "" end
                                if item[shift+2] == nil then item[shift+2] = "" end
                                if item[shift+3] == nil then item[shift+3] = "" end
                                love.graphics.setFont(vcr)
                                love.graphics.printf(
                                    "  "..item[shift].."   "..item[shift+1].."   "..item[shift+2].."   "..item[shift+3],
                                    anchorx+20, anchory+(imgheight/2)-75, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                                love.graphics.setColor(1,1,1)
                                love.graphics.setFont(serif)
                                love.graphics.printf(
                                    spells[item[cursors.combatSpell.v]].desc, anchorx+20, anchory+15, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                                love.graphics.printf(
                                    "Uses "..spells[item[cursors.combatSpell.v]].mind.." Mind", anchorx+20, anchory+45, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                                local cursorsplacement = cursors.combatSpell.v
                                if cursorsplacement > 4 then cursorsplacement = 4 end
                                love.graphics.draw(assets.images.sprites[cont], anchorx+20 + (cursorsplacement-1)*115, anchory+(imgheight/2)-75, 0, 0.5)
                            end
                        end
                    else
                        love.graphics.setFont(serif)
                        if opponentDead then
                            love.graphics.printf(
                            gameState.opponent.name.." has died.", anchorx+20, anchory+15, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                        elseif enemyAttacking then
                            love.graphics.printf(
                            enemyAtckDialogue, anchorx+20, anchory+15, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                        else
                            if cursors.combat.v == 1 then
                                if miss then
                                    love.graphics.printf(
                                    'You missed!', anchorx+20, anchory+15, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                                else
                                    love.graphics.printf(
                                    'You did '..totalDamage..' damage.', anchorx+20, anchory+15, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                                end
                            elseif cursors.combat.v == 2 or cursors.combat.v == 3 then
                                love.graphics.printf(
                                itemspelltext, anchorx+20, anchory+15, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                            else
                                love.graphics.printf(
                                'You couldn\'t get away!', anchorx+20, anchory+15, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                            end
                        end
                    end
                else
                    if cursors.inventoryPage.v == 1 then
                        love.graphics.setColor(1,1,1)
                        love.graphics.setFont(serif)
                        love.graphics.printf(
                            "What would you like to do?", anchorx+20, anchory+15, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                        love.graphics.setColor(0.5,0.5,1)
                        love.graphics.setFont(vcr)
                        love.graphics.printf(
                            "  SCAN    ITEMS   SPELL", 
                            anchorx+20, anchory+(imgheight/2)-75, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                        love.graphics.setColor(1,1,1)
                        love.graphics.draw(assets.images.sprites[cont], anchorx+20 + (cursors.inventory.v-1)*115, anchory+(imgheight/2)-75, 0, 0.5)
                    else
                        if cursors.inventory.v == 2 then
                            love.graphics.setColor(0.5,0.5,1)
                            local item = {}
                            for i=1, #gameState.inventory do
                                item[i] = gameState.inventory[i]
                            end
                            local shift = cursors.inventoryItem.v
                            if shift < 4 then shift = 1 else shift = shift - 3 end
                            if item[shift  ] == nil then item[shift  ] = "NONE" end
                            if item[shift+1] == nil then item[shift+1] = "" end
                            if item[shift+2] == nil then item[shift+2] = "" end
                            if item[shift+3] == nil then item[shift+3] = "" end
                            love.graphics.setFont(vcr)
                            love.graphics.printf(
                                "  "..item[shift].."   "..item[shift+1].."   "..item[shift+2].."   "..item[shift+3],
                                anchorx+20, anchory+(imgheight/2)-75, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                            love.graphics.setColor(1,1,1)
                            love.graphics.setFont(serif)
                            love.graphics.printf(
                                items[item[cursors.inventoryItem.v]].desc, anchorx+20, anchory+15, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                            local cursorsplacement = cursors.inventoryItem.v
                            if cursorsplacement > 4 then cursorsplacement = 4 end
                            love.graphics.draw(assets.images.sprites[cont], anchorx+20 + (cursorsplacement-1)*115, anchory+(imgheight/2)-75, 0, 0.5)
                        elseif cursors.inventory.v == 3 then
                            love.graphics.setColor(0.5,0.5,1)
                            local item = {}
                            for i=1, #gameState.spells do
                                item[i] = gameState.spells[i]
                            end
                            local shift = cursors.inventorySpell.v
                            if shift < 4 then shift = 1 else shift = shift - 3 end
                            if item[shift  ] == nil then item[shift  ] = "NONE" end
                            if item[shift+1] == nil then item[shift+1] = "" end
                            if item[shift+2] == nil then item[shift+2] = "" end
                            if item[shift+3] == nil then item[shift+3] = "" end
                            love.graphics.setFont(vcr)
                            love.graphics.printf(
                                "  "..item[shift].."   "..item[shift+1].."   "..item[shift+2].."   "..item[shift+3],
                                anchorx+20, anchory+(imgheight/2)-75, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                            love.graphics.setColor(1,1,1)
                            love.graphics.setFont(serif)
                            love.graphics.printf(
                                spells[item[cursors.inventorySpell.v]].desc, anchorx+20, anchory+15, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                            love.graphics.printf(
                                "Uses "..spells[item[cursors.inventorySpell.v]].mind.." Mind", anchorx+20, anchory+45, (imgwidth*2)-40, "left", 0, 0.5, 0.5)
                            local cursorsplacement = cursors.inventorySpell.v
                            if cursorsplacement > 4 then cursorsplacement = 4 end
                            love.graphics.draw(assets.images.sprites[cont], anchorx+20 + (cursorsplacement-1)*115, anchory+(imgheight/2)-75, 0, 0.5)
                        end
                    end
                end
                love.graphics.setFont(vcr)
                love.graphics.setColor(1,1,1)
                love.graphics.draw(assets.images.sprites[flesh], anchorx+imgwidth-150, anchory-100, 0, 0.146)
                love.graphics.printf("PARANOIA\n"..gameState.paranoia.."%", anchorx+imgwidth-150, anchory-80, 300, "center", 0, 0.5)
            else
                love.graphics.setFont(vcr)
                love.graphics.draw(assets.images.sprites[flesh], width-170, height-110, 0, 0.146)
                love.graphics.printf("PARANOIA\n"..gameState.paranoia.."%", width-170, height-90, 300, "center", 0, 0.5)
            end
        end)
        if screenState.frozen and not gameState.isInBattle then
            if showControls then
                img = assets.images.sprites[12]
                love.graphics.draw(img, (width/2)-(img:getWidth()/2), (height/2)-(img:getHeight()/2))
            elseif screenState.isInInventory then
                img = assets.images.sprites[15]
                love.graphics.draw(img, (width/2)-(img:getWidth()/2), (height/2)-(img:getHeight()/2))
            else
                img = assets.images.sprites[11]
                love.graphics.draw(img, (width/2)-(img:getWidth()/2), (height/2)-(img:getHeight()/2))
            end
        end
        if not screenState.isInDialogue and not gameState.isInBattle and not screenState.isInInventory then
            if map[gameState.location].leadsTo.left then
                love.graphics.setColor(1,1,1)
                if map[gameState.location].leadsTo.left == gameState.lastRoom then love.graphics.setColor(0.5,0.5,0.5) end
                love.graphics.draw(assets.images.sprites[left], 10, 277.5)
            end
            if map[gameState.location].leadsTo.right then
                love.graphics.setColor(1,1,1)
                if map[gameState.location].leadsTo.right == gameState.lastRoom then love.graphics.setColor(0.5,0.5,0.5) end
                love.graphics.draw(assets.images.sprites[right], 745, 277.5)
            end
            if map[gameState.location].leadsTo.forward then
                love.graphics.setColor(1,1,1)
                if map[gameState.location].leadsTo.forward == gameState.lastRoom then love.graphics.setColor(0.5,0.5,0.5) end
                love.graphics.draw(assets.images.sprites[up], 377.5, 10)
            end
            if map[gameState.location].leadsTo.back then
                love.graphics.setColor(1,1,1)
                if map[gameState.location].leadsTo.back == gameState.lastRoom then love.graphics.setColor(0.5,0.5,0.5) end
                love.graphics.draw(assets.images.sprites[down], 377.5, 545)
            end
        end
    end
end

function loadShaders()
    ambientShader = moonshine(moonshine.effects.vignette).chain(moonshine.effects.chromasep).chain(moonshine.effects.desaturate)
    ambientShader.desaturate.strength = 0
    ambientShader.chromasep.radius = 2
    pausedShader = moonshine(moonshine.effects.vignette).chain(moonshine.effects.dmg)
    pausedShader.dmg.palette = {{0x00, 0x2b, 0x59}, {0x00,0x5f,0x8c}, {0x00, 0xb9, 0xbe}, {0x9f, 0xf4, 0xe5}}
    pixelshader = moonshine(moonshine.effects.pixelate)
    pixelshader.pixelate.size = {3,3}
end