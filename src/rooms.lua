function changeRoom(dir)
    if dir == "right" and map[gameState.location].leadsTo.right then
        gameState.lastRoom = gameState.location
        gameState.location = map[gameState.location].leadsTo.right
        roomEvents()
    end
    if dir == "left" and map[gameState.location].leadsTo.left then
        gameState.lastRoom = gameState.location
        gameState.location = map[gameState.location].leadsTo.left
        roomEvents()
    end
    if dir == "up" and map[gameState.location].leadsTo.forward then
        gameState.lastRoom = gameState.location
        gameState.location = map[gameState.location].leadsTo.forward
        roomEvents()
    end
    if dir == "down" and map[gameState.location].leadsTo.back then
        gameState.lastRoom = gameState.location
        gameState.location = map[gameState.location].leadsTo.back
        roomEvents()
    end
end

function roomEvents()
    playSoundFX(assets.sounds[2])
    local paranoiaModifier
    if gameState.paranoia == 0 then
        paranoiaModifier = 1
    else
        paranoiaModifier = gameState.paranoia/2
    end
    local threshold = map[gameState.location].baseEncounterChance * (paranoiaModifier)
    local roll = math.random(1,100)
    if roll <= threshold then
        engageEncounter()
    end
end