function advanceDialogue(list)
    if gameState.speaking == nil or list then
        if #list == 0 then
            screenState.isInDialogue = false
            do return end
        end
        gameState.speaking = list
        playSoundFX(assets.sounds[gameState.speaking[gameState.speakingIndex].audio])
    elseif gameState.speaking ~= nil and gameState.speaking[gameState.speakingIndex] ~= nil then
        gameState.speakingIndex = gameState.speakingIndex + 1
        if gameState.speaking[gameState.speakingIndex] == nil then
            screenState.isInDialogue = false
            gameState.speaking = nil
            gameState.speakingIndex = 1
        else
            playSoundFX(assets.sounds[gameState.speaking[gameState.speakingIndex].audio])
        end
    end
end