function makeNewGameState()
    return {
        location = "sbedroom",
        inventory = {
            "DOLLR"
        },
        spells = {
            "BRACE",
            "PSYCH",
        },
        speed = 15,
        xp = 0,
        skin = 100,
        mind = 100,
        paranoia = 0,
        weapon = "bat",
        accessory = nil,
        isInBattle = false,
        opponent = {},
        turn = true,
        defeated = {},
        speaking = nil,
        speakingIndex = 1,
        lastRoom = nil,
    }
end