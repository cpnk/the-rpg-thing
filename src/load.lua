function love.load()
    gameState = readSaveData()
    loadAssets()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    loadShaders()
    if gameState.isInBattle then
        setupTurn()
    end
    bgcanvas = love.graphics.newCanvas()
end