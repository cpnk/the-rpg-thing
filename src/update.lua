function love.update(dt)
    flux.update(dt)
    timer.update(dt)
    if screenState.isLoading then
        if locImages:isComplete() and spriteImages:isComplete() and soundDatas:isComplete() then
            --playAudio(assets.sounds[12])
            screenState.isLoading = false
            -- screenState.isMenu = true
        end
    end
    ambientShader.desaturate.strength = gameState.paranoia/100
end