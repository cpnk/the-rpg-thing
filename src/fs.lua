function readSaveData()
    local info = love.filesystem.getInfo('save')
    if info ~= nil then
        return love.filesystem.load('save')()
    else
        playSoundFX(assets.sounds[9])
        screenState.frozen = true
        showControls = true
        freezeTimer = timer.after(math.huge, function()
            screenState.frozen = false
            showControls = false
        end)
        return makeNewGameState()
    end
end

function save()
    love.filesystem.write('save', "--NO TOUCHING!!!\n"..serpent.dump(gameState))
end