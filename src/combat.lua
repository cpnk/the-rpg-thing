function engageEncounter()
    local chosenEnemy = enemies[map[gameState.location].encounters[math.random(#map[gameState.location].encounters)]]
    for i,v in ipairs(gameState.defeated) do
        if chosenEnemy.recursiveName == v then
            do return end
        end
    end
    playSoundFX(assets.sounds[9])
    screenState.frozen = true
    gameState.turn = true
    gameState.opponent = deepcopy(chosenEnemy)
    setupTurn()
    timer.after(1.25, function()
        if gameState.isInBattle then do return end end
        gameState.isInBattle = true
        screenState.isInDialogue = true
        advanceDialogue(gameState.opponent.encounterSpeech,false)
        if gameState.opponent.encounterFunctions then
            for i,v in ipairs(gameState.opponent.encounterFunctions) do
                loadstring(v)()
            end
        end
        timer.after(0.2, function()
            screenState.frozen = false
        end)
    end)
end

function setupTurn()
    enemyAttacking = false
    quip = gameState.opponent.quips[math.random(#gameState.opponent.quips)]
    resetAllCombatCursors()
end

function combatSelect()
    cursors.combatItem.l = #gameState.inventory
    if cursors.combatItem.l == 0 then cursors.combatItem.l = 1 end
    cursors.combatSpell.l = #gameState.spells
    if cursors.combatSpell.l == 0 then cursors.combatSpell.l = 1 end
    if gameState.turn then
        playSoundFX(assets.sounds[6])
        if cursors.combat.v == 1 then
            attack()
            gameState.turn = false
        elseif cursors.combat.v == 2 then
            if cursors.combatPage.v == 1 then cDown('combatPage')
            else
                useItem(false)
            end
        elseif cursors.combat.v == 3 then
            if cursors.combatPage.v == 1 then cDown('combatPage')
            else
                useSpell(false)
            end
        elseif cursors.combat.v == 4 then
            leave()
            gameState.turn = false
        end
    else
        if freezeTimer then
            timer.nvm(freezeTimer)
        end
    end
end

function useItem(cond)
    if items[gameState.inventory[cursors.combatItem.v]] == nil then
        cUp('combatPage')
        do return end
    end
    if items[gameState.inventory[cursors.combatItem.v]].combat == cond then
        playSoundFX(assets.sounds[9])
        do return end
    end
    gameState.turn = false
    items[gameState.inventory[cursors.combatItem.v]].func()
    table.remove(gameState.inventory, cursors.combatItem.v)
    if not cond then checkDead() else
        screenState.isInInventory = false
        resetAllInventoryCursors()
    end
end

function useSpell(cond)
    if spells[gameState.spells[cursors.combatSpell.v]] == nil then
        cUp('combatPage')
        do return end
    end
    if spells[gameState.spells[cursors.combatSpell.v]].combat == cond then
        playSoundFX(assets.sounds[9])
        do return end
    end
    if spells[gameState.spells[cursors.combatSpell.v]].mind <= gameState.mind then
        gameState.mind = gameState.mind - spells[gameState.spells[cursors.combatSpell.v]].mind
    else
        playSoundFX(assets.sounds[9])
        do return end
    end
    gameState.turn = false
    spells[gameState.spells[cursors.combatSpell.v]].func()
    if not cond then checkDead() else
        screenState.isInInventory = false
        resetAllInventoryCursors()
    end
end

function attack()
    local damage = weapons[gameState.weapon].baseDamage
    local roll = math.random(weapons[gameState.weapon].modifier)
    totalDamage = damage+roll
    local speedRoll = math.random(100)
    if gameState.opponent.speed > speedRoll then
        totalDamage = 0
        miss = true
    end
    gameState.opponent.health = gameState.opponent.health - totalDamage
    playSoundFX(assets.sounds[7])
    screenState.frozen = true
    checkDead()
end

function checkDead()
    if gameState.opponent.health <= 0 then
        opponentDead = true
        timer.after(1.5, function()
            screenState.frozen = false
            gameState.isInBattle = false
            screenState.isInDialogue = true
            advanceDialogue({{
                text = "Gained "..gameState.opponent.xp.." XP",
                color = {1,1,1},
                audio = 6
            }});
            gameState.xp = gameState.xp + gameState.opponent.xp
            gameState.defeated[#gameState.defeated+1] = gameState.opponent.recursiveName
            gameState.opponent = nil
        end)
        stopAudio()
    else
        enemyAttack()
    end
end

function leave()
    if gameState.speed > gameState.opponent.run then
        playSoundFX(assets.sounds[8])
        gameState.isInBattle = false
        gameState.opponent = {}
        resetAllCombatCursors()
        stopAudio()
    else
        screenState.frozen = true
        enemyAttack()
    end
end

function enemyAttack()
    timer.after(1.5, function()
        playSoundFX(assets.sounds[6])
        miss = false
        totalDamage = 0
        isBracing = false
        enemyAttacking = true
        enemyAtckDialogue = ""
        local dmg = gameState.opponent.baseAttack + math.random(0,gameState.opponent.baseAttack)
        if isBracing then
            dmg = dmg - 10
            if dmg < 0 then dmg = 0 end
        end
        local missRoll = math.random(100)
        if missRoll <= gameState.speed then
            enemyAtckDialogue = gameState.opponent.name.." swings and misses."
        else
            gameState.skin = gameState.skin - dmg
            enemyAtckDialogue = gameState.opponent.name.." hits you for "..dmg.." damage."
            if gameState.skin <= 0 then
                screenState.gameOver = true
                stopAudio()
            end
        end
        if gameState.opponent.burning then
            gameState.opponent.health = gameState.opponent.health - math.floor(math.random()*10)
            if gameState.opponent.health <= 1 then gameState.opponent.health = 1 end
            enemyAtckDialogue = enemyAtckDialogue .. " They burn."
        end
        timer.after(1.5, function()
            gameState.turn = true
            screenState.frozen = false
            setupTurn()
        end)
    end)
end

function resetAllCombatCursors()
    enemyAttacking = false
    miss = false
    itemspelltext = ""
    isBracing = false
    totalDamage = 0
    opponentDead = false
    resetCursor('combat')
    resetCursor('combatPage')
    resetCursor('combatItem')
    resetCursor('combatSpell')
end