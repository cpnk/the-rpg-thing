assets = {
    fonts = {},
    images = {
        locations = {},
        sprites = {}
    },
    sounds = {}
}

function loadAssets()
    serif = love.graphics.newFont("assets/fonts/EBGaramond-Regular.ttf", 50)
    vcr = love.graphics.newFont("assets/fonts/vcr.ttf", 50);
    love.graphics.setFont(serif)
    local locations = love.filesystem.getDirectoryItems("assets/images/locations")
    for i,v in ipairs(locations) do
        locations[i] = {"newImage", "assets/images/locations/"..v}
    end
    locImages = lily.loadMulti(locations)
    locImages:onComplete(function(_, lilies)
		for i,v in ipairs(lilies) do
            assets.images.locations[i] = v[1]
        end
	end)
    local sprites = love.filesystem.getDirectoryItems("assets/images/sprites");
    for i,v in ipairs(sprites) do
        sprites[i] = {"newImage", "assets/images/sprites/"..v}
    end
    spriteImages = lily.loadMulti(sprites);
    spriteImages:onComplete(function(_, lilies)
        for i,v in ipairs(lilies) do
            assets.images.sprites[i] = v[1]
        end
    end)
    local sounds = love.filesystem.getDirectoryItems("assets/audio");
    for i,v in ipairs(sounds) do
        sounds[i] = {"newSource", "assets/audio/"..v, "static"}
    end
    soundDatas = lily.loadMulti(sounds);
    soundDatas:onComplete(function(_, lilies)
        for i,v in ipairs(lilies) do
            assets.sounds[i] = v[1]
        end
    end)
end