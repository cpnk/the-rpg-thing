spells = {
    NONE = {
        desc = "You have no spells"
    },
    BRACE = {
        desc = "Brace for impact",
        func = function()
            isBracing = true
            itemspelltext = "You anticipated impact."
        end,
        mind = 0,
        combat = true
    },
    PSYCH = {
        desc = "Psych out, deal damage depending on your paranoia",
        func = function()
            local damage = math.ceil((math.random()*gameState.paranoia)/2)
            gameState.opponent.health = gameState.opponent.health - damage
            itemspelltext = "Did "..damage.." psychic damage."
        end,
        mind = 10,
        combat = true
    }
}
