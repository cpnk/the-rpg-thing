items = {
    NONE = {
        desc = "You have no items."
    },
    ["LWEXE"] = {
        desc = "Limewire.exe - causes opponent's speed to drop to 0",
        combat = true,
        func = function()
            gameState.opponent.speed = 0
            itemspelltext = "Opponent started lagging."
        end
    },
    ["E.NFT"] = {
        desc = "Ethereum NFT - inflicts burning damage",
        combat = true,
        func = function()
            gameState.opponent.burning = true
            itemspelltext = "Opponent started burning."
        end
    },
    ["DOLLR"] = {
        desc = "United States Dollar - Teleports you to an unknown location",
        combat = false,
        func = function()
            gameState.lastRoom = gameState.location
            gameState.location = "convenience"
            roomEvents()
        end,
        sprite = 14
    }
}

function resetAllInventoryCursors()
    resetCursor('inventory')
    resetCursor('inventoryPage')
    resetCursor('inventoryItem')
    resetCursor('inventorySpell')
end

function inventorySelect()
    cursors.inventoryItem.l = #gameState.inventory
    if cursors.inventoryItem.l == 0 then cursors.inventoryItem.l = 1 end
    cursors.inventorySpell.l = #gameState.spells
    if cursors.inventorySpell.l == 0 then cursors.inventorySpell.l = 1 end
    playSoundFX(assets.sounds[6])
    if cursors.inventory.v == 1 then
        scan()
    elseif cursors.inventory.v == 2 then
        if cursors.inventoryPage.v == 1 then cDown('inventoryPage')
        else
            useItem(true)
        end
    elseif cursors.inventory.v == 3 then
        if cursors.inventoryPage.v == 1 then cDown('inventoryPage')
        else
            useSpell(true)
        end
    end
end