enemies = {
    none = nil,
    slimshady = {
        name = "Slim",
        recursiveName = "slimshady",
        health = 100,
        sprite = 7,
        speed = 5,
        run = 35,
        baseAttack = 5,
        xp = 15,
        encounterSpeech = {
            {
                text = "Your arms look heavy...",
                color = {1,0,0},
                audio = 3
            },
            {
                text = "Let me relieve you of that burden.",
                color = {1,0,0},
                audio = 3
            }
        },
        quips = {
            "* Slim looks straight through you.",
            "* Slim looks determined.",
            "* Slim begins beatboxing."
        }
    },
    possum = {
        name = "Possum",
        recursiveName = "possum",
        health = 5,
        sprite = 10,
        speed = 0,
        run = 0,
        baseAttack = 0,
        xp = 1,
        encounterSpeech = {
            {
                text = "What are dreams but the manifest of ourselves?",
                color = {0,0,0},
                audio = 11
            },
            {
                text = "What the hell are you talking about?",
                color = {1,1,1},
                audio = 1
            },
            {
                text = "You're a long ways from home, boy.",
                color = {0,0,0},
                audio = 11
            },
        },
        quips = {
            "* Possum stares at you.",
            "* Possum doesn't move.",
            "* Possum is not blinking."
        },
        encounterFunctions = {
            "raiseParanoia(10)"
        }
    },
    prunsel = {
        name = "Prunsel",
        recursiveName = "prunsel",
        health = 50,
        sprite = 16,
        speed = 20,
        run = 50,
        baseAttack = 7,
        xp = 15,
        encounterSpeech = {},
        quips = {
            "* Prunsel says nothing.",
            "* Prunsel looks at you.",
            "* Prunsel floats in place."
        },
        encounterFunctions = {
            "raiseParanoia(2)",
            "playAudio(assets.sounds[12])"
        }
    }
}

function raiseParanoia(val)
    gameState.paranoia = gameState.paranoia + val
    if gameState.paranoia > 100 then
        gameState.paranoia = 100
    end
end

function scan()
    screenState.frozen = true
    freezeTimer = nil
    timer.after(1.5, function()
        screenState.frozen = false
        screenState.isInInventory = false
        local found = false
        local chosenEnemy = map[gameState.location].encounters[1]
        if chosenEnemy ~= nil then
            found = true
            chosenEnemy = enemies[chosenEnemy]
            for i,v in ipairs(gameState.defeated) do
                if chosenEnemy.recursiveName == v then
                    do return end
                end
            end
        end
        if not found then
            screenState.isInDialogue = true
            advanceDialogue({
                {
                    text = "Found nothing.",
                    color = {1,1,1},
                    audio = 1
                }
            })
        else
            engageEncounter()
        end
    end)
end