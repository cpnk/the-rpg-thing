function love.keypressed(key)
    if screenState.gameOver then
        if key == "escape" or key == "space" or key == "return" then
            resetAllCombatCursors()
            screenState = deepcopy(startingState)
            love.load()
        end
    end

    if screenState.frozen then
        if key == "space" or key == "return" then
            if freezeTimer then
                timer.nvm(freezeTimer)
            end
            do return end
        else do return end end
    end

    if key == "escape" and not screenState.isInDialogue and not screenState.isMenu then
        screenState.isPaused = not screenState.isPaused
        resetCursor('pause')
    end

    if key == "space" or key == "return" then
        actionSelect()
    end
    
    if key == "rshift" or key == "lshift" then
        actionBack()
    end

    if key == "up" or key == "down" or key == "left" or key == "right" then
        directional(key)
    end
end

function actionBack()
    if gameState.isInBattle and not screenState.isPaused and not screenState.isMenu then
        resetCursor('combatPage')
    end

    if not screenState.isInDialogue and not screenState.isLoading and not screenState.isPaused and not screenState.isMenu and not gameState.isInBattle and not screenState.isInInventory then
        screenState.isInInventory = true
    elseif screenState.isInInventory and not screenState.isPaused then
        if cursors.inventoryPage.v == 1 then
            screenState.isInInventory = false
            resetAllInventoryCursors()
        else
            cUp("inventoryPage")
        end
    end
end

function actionSelect()
    if not screenState.isInDialogue and not screenState.isLoading and not screenState.isPaused and not screenState.isMenu and not gameState.isInBattle and not screenState.isInInventory then
        screenState.isInDialogue = true
        advanceDialogue(map[gameState.location].search)
    elseif screenState.isInDialogue and not screenState.isPaused then
        advanceDialogue()
    elseif screenState.isInInventory and not screenState.isPaused and not screenState.frozen then
        inventorySelect()
    elseif gameState.isInBattle and not screenState.isPaused and not screenState.isMenu then
        combatSelect()
    end
    if screenState.isPaused then
        playSoundFX(assets.sounds[6])
        if cursors.pause.v == 1 then
            screenState.isPaused = false
        elseif cursors.pause.v == 2 then
            save()
        else
            love.event.quit()
        end
    end
end

function directional(dir)
    if not screenState.isInDialogue and not screenState.isLoading and not screenState.isPaused and not screenState.isMenu and not gameState.isInBattle and not screenState.isInInventory then
        changeRoom(dir)
    end
    if not screenState.isInDialogue and not screenState.isLoading and not screenState.isPaused and not screenState.isMenu and gameState.isInBattle and gameState.turn then
        local page = "combat"
        if cursors.combatPage.v == 2 then
            if cursors.combat.v == 2 then
                page = "combatItem"
            elseif cursors.combat.v == 3 then
                page = "combatSpell"
            end
        end
        if dir == "left" then
            cLeft(page)
            playSoundFX(assets.sounds[5])
        elseif dir == "right" then
            cRight(page)
            playSoundFX(assets.sounds[5])
        end
    end
    if not screenState.isInDialogue and not screenState.isLoading and not screenState.isPaused and not screenState.isMenu and screenState.isInInventory then
        local page = "inventory"
        if cursors.inventoryPage.v == 2 then
            if cursors.inventory.v == 2 then
                page = "inventoryItem"
            elseif cursors.inventory.v == 3 then
                page = "inventorySpell"
            end
        end
        if dir == "left" then
            cLeft(page)
            playSoundFX(assets.sounds[5])
        elseif dir == "right" then
            cRight(page)
            playSoundFX(assets.sounds[5])
        end
    end
    if screenState.isPaused then
        if dir == "up" then
            cUp("pause")
            playSoundFX(assets.sounds[5])
        elseif dir == "down" then
            cRight("pause")
            playSoundFX(assets.sounds[5])
        end
    end
end