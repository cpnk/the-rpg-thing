map = {
    -- spawn
    sbedroom = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 52,
        search = {
            {
                text = "Good morning, USA",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            right = "shallway"
        }
    },
    shallway = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 49,
        search = {
            {
                text = "The hallway is void of light. It taunts me.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            forward = "slivingroom",
            back = "sbedroom"
        }
    },
    slivingroom = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 35,
        search = {
            {
                text = "Smells of mold.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            forward = "shallway",
            back = "roadtonowhere",
            right = "sbackyard"
        }
    },
    sbackyard = {
        baseEncounterChance = 112,
        encounters = {"possum"},
        bgImg = 41,
        search = {
            {
                text = "What a nice yard. Something feels off though...",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "slivingroom"
        }
    },
    roadtonowhere = {
        baseEncounterChance = 40,
        encounters = {"prunsel"},
        bgImg = 53,
        search = {
            {
                text = "The pastel colors sicken me.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            right = "slivingroom",
            back = "playgroundGate",
            forward = "backyardDweller"
        }
    },
    backyardDweller = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 71,
        search = {
            {
                text = "I'm sure the people around here wont mind if I check out their yards.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "roadtonowhere",
            forward = "mansion"
        }
    },
    mansion = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 72,
        search = {
            {
                text = "The terrain here pulls me in every direction.",
                color = {1,1,1},
                audio = 1
            },
            {
                text = "I'm going to fall.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "backyardDweller",
            forward = "livingRoom",
            right = "bliss"
        }
    },
    bliss = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 70,
        search = {
            {
                text = "Reminds me of a certain operating system.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "mansion",
            forward = "roundabout"
        }
    },
    roundabout = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 77,
        search = {
            {
                text = "A little neighborhood.",
                color = {1,1,1},
                audio = 1
            },
            {
                text = "Cringe.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "bliss",
            forward = "angel"
        }
    },
    angel = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 60,
        search = {
            {
                text = "This one glows.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "roundabout"
        }
    },
    livingRoom = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 42,
        search = {
            {
                text = "Red lights have always soothed me, but this is a little unnerving",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            left = "mansion",
            right = "window"
        }
    },
    window = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 46,
        search = {
            {
                text = "I'm reminded of my days as a child, rotting in a building of brick.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            left = "livingRoom"
        }
    },
    playgroundGate = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 12,
        search = {
            {
                text = "A children's playground.",
                color = {1,1,1},
                audio = 1
            },
            {
                text = "I hate it here.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "roadtonowhere",
            right = "dock",
            left = "play"
        }
    },
    dock = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 5,
        search = {
            {
                text = "The masculine urge to grab a fish with my bare hands...",
                color = {1,1,1},
                audio = 1
            },
            {
                text = "...is not very strong.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "playgroundGate"
        }
    },
    play = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 25,
        search = {
            {
                text = "The swings screech my name.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "playgroundGate",
            left = "gr",
            forward = "ou",
            right = "nd"
        }
    },
    gr = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 22,
        search = {
            {
                text = "I love the taste of woodchips!",
                color = {1,1,1},
                audio = 1
            },
            {
                text = "Way better than my cooking.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "play"
        }
    },
    ou = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 23,
        search = {
            {
                text = "I can feel the static electricity from that slide, and I'm nowhere close to it.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "play"
        }
    },
    nd = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 24,
        search = {
            {
                text = "I really want to go climb on that right now.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "play"
        }
    },
    convenience = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 76,
        search = {
            {
                text = "O sweet orange haze, please provide me with shelter!",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            forward = "doors"
        }
    },
    doors = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 29,
        search = {
            {
                text = "Ah, the sweet smell of a push door.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            forward = "convenience",
            back = "reception"
        }
    },
    reception = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 33,
        search = {
            {
                text = "I'm here to see a patient...",
                color = {1,1,1},
                audio = 1,
            },
            {
                text = "She had a miscarriage.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "doors",
            forward = "halls",
            right = "meeting"
        }
    },
    meeting = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 6,
        search = {
            {
                text = "It seems whoever was here left in a hurry.",
                color = {1,1,1},
                audio = 1,
            }
        },
        leadsTo = {
            back = "reception"
        }
    },
    halls = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 64,
        search = {
            {
                text = "Each and every one of these tiles are made of 100% asbestos.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "reception",
            forward = "halls2"
        }
    },
    halls2 = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 28,
        search = {
            {
                text = "I'm feeling an onset of mesothelioma!",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "halls",
            left = "halls3"
        }
    },
    halls3 = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 15,
        search = {
            {
                text = "Smells like old people.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "halls2",
            left = "backroomEntrance",
            forward = "poolsEntrance"
        }
    },
    backroomEntrance = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 61,
        search = {
            {
                text = "It's really quiet here.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "halls3",
            right = "backrooms"
        }
    },
    backrooms = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 36,
        search = {
            {
                text = "Humming fluorescent lights above me scream like a lost child.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "backroomEntrance",
            forward = "backrooms2"
        }
    },
    backrooms2 = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 67,
        search = {
            {
                text = "This carpet is more mold than carpet.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "backrooms",
            forward = "backrooms3"
        }
    },
    backrooms3 = {
        baseEncounterChance = 50,
        encounters = {
            "slimshady"
        },
        bgImg = 66,
        search = {
            {
                text = "Is there anybody...",
                color = {1,1,1},
                audio = 1
            },
            {
                text = "...out there?",
                color = {1,1,1},
                audio = 13
            }
        },
        leadsTo = {
            back = "backrooms2"
        }
    },
    poolsEntrance = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 37,
        search = {
            {
                text = "The scent of chlorine surrounds me!",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "halls3",
            left = "cgpool"
        }
    },
    cgpool = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 3,
        search = {
            {
                text = "I wonder how much water this place uses.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "poolsEntrance",
            left = "greenwater"
        }
    },
    greenwater = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 10,
        search = {
            {
                text = "Getting some real \"A cure for wellness\" vibes.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "cgpool",
            right = "bathouse"
        }
    },
    bathouse = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 14,
        search = {
            {
                text = "These guys must be really big clean freaks.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "greenwater",
            forward = "octopus"
        }
    },
    octopus = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 48,
        search = {
            {
                text = "Yo! They have a waterpark here too!",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "bathouse",
            right = "lappool",
            left = "hotelpool"
        }
    },
    lappool = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 54,
        search = {
            {
                text = "I bet I could beat anyone in a race here.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "octopus"
        }
    },
    hotelpool = {
        baseEncounterChance = 0,
        encounters = {},
        bgImg = 38,
        search = {
            {
                text = "The rooms here probably all smell like pool water.",
                color = {1,1,1},
                audio = 1
            }
        },
        leadsTo = {
            back = "octopus"
        }
    },
}