function playSoundFX(audio)
    if audio == nil then do return end end
    audio:stop()
    audio:play()
end

local currentlyplaying = nil

function playAudio(audio)
    if audio == nil then do return end end
    if currentlyplaying ~= nil then
        currentlyplaying:stop()
    end
    currentlyplaying = audio
    currentlyplaying:play()
    currentlyplaying:setLooping(true)
end

function stopAudio()
    if currentlyplaying ~= nil then
        currentlyplaying:stop()
    end
end