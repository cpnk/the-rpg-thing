math.randomseed(os.time())
serpent = require "libs.serpent"
moonshine = require "libs.moonshine"
lily = require "libs.lily"
flux = require "libs.flux"
timer = require "libs.timer"
require "libs.deepcopy"
require "src.fs"
require "src.map"
require "src.gamestate"
require "src.draw"
require "src.load"
require "src.assets"
require "src.update"
require "src.input"
require "src.dialogue"
require "src.rooms"
require "src.combat"
require "src.enemies"
require "src.audio"
require "src.cursors"
require "src.weapons"
require "src.items"
require "src.spells"

gameState = {}

startingState = {
    frozen = false,
    isPaused = false,
    isMenu = false,
    isInDialogue = false,
    isLoading = true,
    gameOver = false
}

screenState = deepcopy(startingState)